'use strict';

exports.activity = function(db) { return require('./models/activity')(db); };

exports.booking = function(db) { return require('./models/booking')(db); };

exports.contact = function(db) { return require('./models/contact')(db); };

exports.creditcard = function(db) { return require('./models/creditcard')(db); };

exports.hotel = function(db) { return require('./models/hotel')(db); };

exports.location = function(db) { return require('./models/location')(db); };

exports.offer = function(db) { return require('./models/offer')(db); };

exports.payment = function(db) { return require('./models/payment')(db); };

exports.user = function(db) { return require('./models/user')(db); };

exports.campaign = function(db) { return require('./models/campaign')(db); };
