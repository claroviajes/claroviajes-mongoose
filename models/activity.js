'use strict';

var mongoose = require('mongoose'),
	mongoosePaginate = require('mongoose-paginate'),
	Schema = mongoose.Schema;

var ActivitySchema = new Schema({
	type: String,
	title: String,
	text: String,
	createdOn: { type: Date, default: Date.now },
	createdBy: { type: Schema.Types.ObjectId, ref: 'User' },
	booking: { type: Schema.Types.ObjectId, ref: 'Booking' },
	contact: { type: Schema.Types.ObjectId, ref: 'Contact' },
	payload: Schema.Types.Mixed
});

ActivitySchema.plugin(mongoosePaginate);

module.exports = function(db) {
	return db.model('Activity', ActivitySchema);
}
