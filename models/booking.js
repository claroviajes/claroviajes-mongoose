'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
	moment = require('moment');

var RoomSchema = new Schema({
	roomName: { type: String, required: true },
	roomCode: { type: String, required: true },
	serviceName: { type: String, required: true },
	serviceCode: { type: String, required: true },
	ratePerNight: { type: Number, required: true },
	nightCount: { type: Number, required: true },
	maxOccupants: { type: Number, required: true }
});

var BookingSchema = new Schema({
	checkin: { type: Date, required: true },
	checkout: { type: Date, required: true },
	rooms: { type: [ RoomSchema ], required: true },
	price: { type: Number, default: 0 },
	subsidy: { type: Number, default: 0 },
	cost: { type: Number, default: 0 },
	adultCount: { type: Number, default: 0 },
	childCount: { type: Number, default: 0 },
	infantCount: { type: Number, default: 0 },
	externalLocator: { type: String, required: true },
	travelOperator: { type: String, required: true },
	observations: String,
	country: String,
	status: { type: String, default: 'new' },
	createdBy: { type: Schema.Types.ObjectId, ref: 'User', required: true },
	createdOn: { type: Date, default: Date.now },
	updatedOn: { type: Date, default: Date.now },
	hotel: { type: Schema.Types.ObjectId, ref: 'Hotel', required: true },
	customer: { type: Schema.Types.ObjectId, ref: 'Contact', required: true },
	payments: [{ type: Schema.Types.ObjectId, ref: 'Payment' }],
	passengers: [{
		firstName: String,
		lastName: String,
		age: Number
	}]
});

BookingSchema
	.path('status')
	.validate(function(status) {
		return status === "new" || status === "confirmed" || status === "requested";
	}, 'Invalid value for status');

BookingSchema
	.path('checkin')
	.validate(function(checkin) {
		return moment(new Date(checkin)).isValid && moment(new Date(checkin)).isBefore(this.checkout);
	}, 'Invalid checkin value');

BookingSchema
	.path('checkout')
	.validate(function(checkout) {
		return moment(new Date(checkout)).isValid && moment(new Date(checkout)).isAfter(this.checkin);
	}, 'Invalid checkout value');

module.exports = function(db) {
	return db.model('Booking', BookingSchema);
}
