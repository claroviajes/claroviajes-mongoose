'use strict';

var mongoose = require('mongoose'),
	mongoosePaginate = require('mongoose-paginate'),
	Schema = mongoose.Schema;

var CampaignSchema = new Schema({
	name: { type: String, required: true },
	startsOn: { type: Date, required: true },
	endsOn: { type: Date, required: true },
	country: { type: String, required: true },
	budget: { type: Number, required: true },
	benefits: [{
		name: { type: String, required: true },
		type: { type: String, required: true },
		unitPrice: { type: Number, required: true },
		slots: { type: Number, required: true }
	}],
	expenses: [{
		type: { type: String, required: true },
		amount: { type: Number, required: true },
		date: { type: Date, required: true }
	}],
	createdBy: { type: Schema.Types.ObjectId, ref: 'User', required: true },
	createdOn: { type: Date, default: Date.now }
});

CampaignSchema.plugin(mongoosePaginate);

module.exports = function(db) {
	return db.model('Campaign', CampaignSchema);
}
