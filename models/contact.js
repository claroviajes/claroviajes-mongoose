'use strict';

var mongoose = require('mongoose'),
	mongoosePaginate = require('mongoose-paginate'),
	Schema = mongoose.Schema;

var ContactSchema = new Schema({
	firstName: String,
	lastName: String,
	organization: String,
	role: String,
	email: String,
	creditcards: [{ type: Schema.Types.ObjectId, ref: 'Creditcard' }],
	subscriptionStatus: String,
	country: String,
	phoneNumber: String,
	whitepagesId: String,
	updatedOn: Date
});

ContactSchema.plugin(mongoosePaginate);

module.exports = function(db) {
	return db.model('Contact', ContactSchema);
}
