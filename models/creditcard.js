'use strict';

var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

var CreditcardSchema = new Schema({
	firstName: String,
	lastName: String,
	country: String,
	address1: String,
	address2: String,
	type: String,
	number: String,
	expiration: String,
	securityCode: String
});

module.exports = function(db) {
	return db.model('Creditcard', CreditcardSchema);
}
