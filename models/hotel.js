'use strict';

var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

var HotelSchema = new Schema({
	name: { type: String, required: true },
	description: String,
	rate: {
		value: Number,
		scratchedValue: Number,
		taxes: Number,
		currencyCode: String
	},
	thumbnail: { type: String },
	images: [{ type: String }],
	stars: Number,
	handle: {
		tripadvisor: String,
		almundo: String,
		booking: String
	},
	services: [ String ],
	geoLocation: {
		latitude: Number,
		longitude: Number
	},
	address: {
		streetAddress: String,
		extendedAddress: String,
		locality: String,
		region: String,
		postalCode: String,
		countryCode: String,
		country: String
	}
});

module.exports = function(db) {
	return db.model('Hotel', HotelSchema);
}
