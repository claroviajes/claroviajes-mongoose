'use strict';

var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

var LocationSchema = new Schema({
	name: { type: String, required: true },
	tripAdvisorId: { type: String },
	lat: { type: String },
	lng: { type: String }
});

module.exports = function(db) {
	return db.model('Location', LocationSchema);
}
