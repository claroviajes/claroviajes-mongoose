'use strict';

var mongoose = require('mongoose'),
	mongoosePaginate = require('mongoose-paginate'),
	Schema = mongoose.Schema;

var OfferSchema = new Schema({
	type: { type: String, required: true },
	title: { type: String, required: true },
	subTitle: { type: String },
	image: { type: String },
	rate: {
		price: Number,
		finalPrice: Number,
		maxPrice: Number,
		slots: { type: Number, default: 0 }
	},
	services: [{ type: String }],
	expiration: Date,
	conditions: [{ type: String }],
	quote: {
		text: String,
		from: String
	},
	detail: Schema.Types.Mixed,
	campaign: { type: Schema.Types.ObjectId, ref: 'Campaign' }
});

OfferSchema
	.path('type')
	.validate(function(type) {
		return type === "hotel" || type === "flight" || type === "tour" || type === "car" || type === "crouise";
	}, 'Invalid value for type');

OfferSchema.plugin(mongoosePaginate);

module.exports = function(db) {
	return db.model('Offer', OfferSchema);
}
