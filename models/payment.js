'use strict';

var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

var PaymentSchema = new Schema({
	createdOn: { type: Date, default: Date.now },
	createdBy: { type: Schema.Types.ObjectId, ref: 'User' },
	amount: Number,
	status: String,
	type: String,
	date: Date,
	contact: { type: Schema.Types.ObjectId, ref: 'Contact' },
	creditcard: { type: Schema.Types.ObjectId, ref: 'Creditcard' }
});

module.exports = function(db) {
	return db.model('Payment', PaymentSchema);
}
