'use strict';

var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

var UserSchema = new Schema({
	email: String,
	lastName: String,
	firstName: String,
	googleId: String,
	picture: String
});

UserSchema
	.virtual('fullName')
	.get(function() {
		return this.firstName + " " + this.lastName;
	});

module.exports = function(db) {
	return db.model('User', UserSchema);
}
